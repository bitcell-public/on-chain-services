import requests

#---------------
# post vote: title and options
# create new random voteid
r = requests.post('http://128.199.76.176:7001/vote/create/0', json = {
    'title':'测试投票000',
    'options': ['1st','2nd','3rd','4th']
    })

print(r.status_code, r.text)


#---------------
# post vote: update title and options
# reset specified voteid
r = requests.post('http://128.199.76.176:7001/vote/create/11', json = {
    'title':'测试投票001',
    'options': ['1st','2nd','3rd','4th']
    })

print(r.status_code, r.text)


#---------------
# post vote data: user id and their choices
r = requests.post('http://128.199.76.176:7001/vote/commit/11', json = {
    101: [1, 3],
    102: [4],
    103: [1, 2, 4, 2],
    104: [1],
    })

print(r.status_code, r.text)


#---------------
# get vote result by voteID
r = requests.get('http://128.199.76.176:7001/vote/result/11')
print(r.status_code, r.text)


#---------------
# Test Output
#---------------
#   200 vote(1) has been saved into a public blockchain.
#   200 vote (1) data updated.
#   200 vote (1) result fetched.
#    (highest:1, fulldata:[2 3 1 0])
