# 测试地址

    128.199.76.176:7001

# 投票接口

## 新建/更新投票活动 `POST /vote/create/VOTEID`

创建或更新独立投票活动。

URL中参数VOTEID为最大64bit整数: 34251

### Params
```javascript
{
  "title": "测试投票000",
  "options": ["1st","2nd","3rd","4th"]
}
```

* voteid: 必选，为64bit整数，如果为0，系统将随机分配一个新的投票ID；如果不为0，则会修改此投票ID上的数据。
* tilte: 必选，设置投票标题。
* options: 必选，为此投票的备选项。

### Response

    curl -s -X POST -H "'Content-type':'application/json, 'charset':'utf-8', 'Accept': 'text/plain'" -d '{"title":"测试投票000","options": ["1st","2nd","3rd","4th"]}' http://localhost:7001/vote/create/34251

```javascript
{
  "code": 0,
  "message": "ok",
  "data": {
    "id": 34251
  }
}
```

## 更新个人投票数据 `POST /vote/commit/VOTEID`

个人投票。

URL中参数VOTEID为最大64bit整数: 34251

### Params
```javascript
{
    101: [1, 3],
    102: [4],
    103: [1, 2, 4],
    104: [1]
}
```
内容为一个map，key是个人userid，value为其投出的投票选项。每个userid可以投多个备选，每个备选只能投1票。

* voteid: 必选，为64bit整数。必须已经创建，否则将投票失败

### Response

    curl -s -X POST -H "'Content-type':'application/json, 'charset':'utf-8', 'Accept': 'text/plain'" -d '{ 101: [1, 3], 102: [4], 103: [1, 2, 4], 104: [1]}' http://localhost:7001/vote/commit/34251

```javascript
{
  "code": 0,
  "message": "ok",
  "data": "vote(34251) data updated."
}
```


## 获取投票结果 `GET /vote/result/VOTEID`

URL中参数VOTEID为最大64bit整数: 34251

### Params

* voteid: 必选，为64bit整数。必须已经创建，否则将投票失败

### Response

    curl -s http://localhost:7001/vote/result/34251

```javascript
{
  "code": 0,
  "message": "ok",
  "data":{
    "title": "测试投票001",
    "result": {
        "1st": 3,
        "2nd": 1,
        "3rd": 1,
        "4th": 2
    }
  }
}
```


# 下层接口
## 新建/更新数据 `POST /set/KEY`

创建或更新数据，并可附带设置数据的一组tags。

URL中参数KEY形如：a1e6e8cd91eb4c1cac4f

### Params
```javascript
{
  "value": "9d4e2e3657e1db7b38026cf3da1e6e8cd91eb4c1cac4fd0012102986",
  "tags": ["first", "test"]
}
```
其中key和value内容都以16进制编码。tags为ASCII文本字符。
* key: 必选，长度至少为1字节，即2个16进制数；最大32字节。
* value: 必选，长度至少为0字节；最大256字节。
* tag: 可选，单个tag长度为1到16个字符；最多设置8个tag。若指定该字段内容，将替换原有tags。

### Response

    curl -s -X POST -H "'Content-type':'application/json, 'charset':'utf-8', 'Accept': 'text/plain'" -d '{"value":"9d4e2e3657e1db7b38026cf3da1e6e8cd91eb4c1cac4fd0012102986","tags": ["first", "test"]}' http://localhost:7001/set/a1e6e8cd91eb4c1cac4f

```javascript
{
  "code": 0,
  "message": "ok",
  "data": {
    "createtime": 1511408898,
    "updatetime": 1514180988,
  }
}
```


## 设置数据的tags `POST /settags/KEY`

设置数据的tag之前必须首先创建相应数据。

URL中参数KEY形如：a1e6e8cd91eb4c1cac4f

### Params
```javascript
{
  "tags": ["first", "test"]
}
```

其中key内容以16进制编码。tags为ASCII文本字符。

* key: 必选，长度至少为1字节，即2个16进制数；最大32字节。
* tag: 必选，单个tag长度为1到16个字符；最多设置8个tag。该字段内容将替换原有tags。

    curl -i -X POST -H "'Content-type':'application/json, 'charset':'utf-8', 'Accept': 'text/plain'" -d '{"tags": ["second", "test2"]}' http://localhost:7001/settags/a1e6e8cd91eb4c1cac4f

### Response
```javascript
{
  "code": 0,
  "message": "ok",
  "data": {
    "updatetime": 1514180988,
  }
}
```


## 获取数据 `GET /get/KEY`
### Params

URL中参数KEY形如：a1e6e8cd91eb4c1cac4f

* key: 必选，指定key，获取其对应的value数据内容，和相关的tags。

返回value内容以16进制编码。

    curl -s http://localhost:7001/get/a1e6e8cd91eb4c1cac4f

### Response
```javascript
{
  "code": 0,
  "message": "ok",
  "data": {
    "tags": ["first", "test"],
    "value": "9d4e2e3657e1db7b38026cf3da1e6e8cd91eb4c1cac4fd0012102986",
    "createtime": 1511408898,
    "updatetime": 1514180988,
  }
}
```

## 查找相关keys `GET /getkeys?tag=hello&value_contains=2102986e`

* tag：可选，过滤出包含此tag的keys。
* value_contains：可选，过滤出value值包含value_contains的keys。

若不指定参数，将返回所有keys。

    curl -s http://localhost:7001/getkeys?tag=test

### Response
```javascript
{
  "code": 0,
  "message": "ok",
  "data": [
    "a1e6e8cd91eb4c1cac4f",
    "4c1cac4fde4324ba"
  ]
}
```
